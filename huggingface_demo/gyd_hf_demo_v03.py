# 基于Gradio YOLOv5 Det v0.3 HuggingFace Demo的自定义接口脚本
# 创建人：曾逸夫
# 创建时间：2022-05-27

import os

os.system("pip install gradio==3.0.6")

import gradio as gr

demo = gr.Interface.load(name="spaces/Zengyf-CVer/Gradio_YOLOv5_Det_v3")

demo.launch()
