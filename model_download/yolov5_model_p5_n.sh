# YOLOv5 P5_n模型下载脚本
# 创建人：曾逸夫
# 创建时间：2022-06-01

cd ./models

yolov5_version="v6.1"
wget_download="wget -c -t 0 https://github.com/ultralytics/yolov5/releases/download/"
model_list=(yolov5n)

for i in ${model_list[*]}; do
$wget_download$yolov5_version$"/"$i$".pt"
echo $i"模型下载成功！"
done
